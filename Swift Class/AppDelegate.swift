//
//  AppDelegate.swift
//  Swift Class
//
//  Created by Joel Rennich on 5/29/18.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

