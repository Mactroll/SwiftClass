#  Find current console user in both Shell and Swift

## Bash

* Bash -> Python -> PyObjC Bridge -> ObjC
* Overhead is high, but probably doesn't matter

## Swift

* All native
* Simple, as it's all built-in
* Will result in an 8 MB binary

