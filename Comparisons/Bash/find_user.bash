#!/bin/sh

#  find_user.bash
#  Swift Class
#
#  Created by Joel Rennich on 6/3/18.
#  Copyright © 2018 Orchard & Grove Inc. All rights reserved.

#!/bin/bash

loggedInUser=$(/usr/bin/python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");')

echo $loggedInUser
