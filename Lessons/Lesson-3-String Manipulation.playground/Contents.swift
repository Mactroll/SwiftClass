// Lesson 3 - String Manipulation
// A really nice feature set of Swift

import Cocoa

// get a nice long string
// triple quotes make pasting in big chunks of text easy

let policy = """
Orchard & Grove, Inc. (“O&G”) is committed to the privacy of your information. This Privacy Policy is meant to help you understand the information we collect, why we collect it, and how you can manage it. This Policy applies to our website at www.nomad.menu (the “Site”) and any NoMAD-related products, services, communications, or apps (the “Services”). By using our Site, or by purchasing our products and services, you are acknowledging that you have read, understood, and accept this Privacy Policy.
In this Privacy Policy, any use of the words "you", "yours," or similar expressions refers to users of this Site and our Services, as well as any other individuals whose information we collect and process. References to "we", "us, "our" or similar expressions refer to NoMAD.
This Policy does not extend to anyone whose personal information is not under our control or management, including data that is collected by other websites that you may visit before or after this Site, and which are not governed by this Privacy Policy. We are not responsible for the data protection or privacy policies of any other websites, and accept no responsibility or liability for those policies. Please carefully check the policies of other websites and services you use before you visit or submit personal information.
"""

print(policy)

// split by paragraphs

let paragraphs = policy.components(separatedBy: CharacterSet.newlines)
print("There are \(paragraphs.count) paragraphs")

// split by sentences
// note this isn't entirely accurate b/c of items like menu.nomad

let sentences = policy.components(separatedBy: ".")
print("There are \(sentences.count) sentences")

// fun with packing this all up into something

var text = [ Int : [ Int : [String]]]()

for i in 0...(paragraphs.count - 1) {
    
    var lines = [ Int : [String]]()
    
    for x in 0...((paragraphs[i].components(separatedBy: ".")).count - 1 ) {

        let sentence = (paragraphs[i].components(separatedBy: "."))[x].components(separatedBy: " ")
        
        lines[x] = sentence
    }
    
    text[i] = lines
}

print(text)

// replacing things

var newText = """
The Halifax Rules:

1) The game was played with a block of wood for a puck.
2) The puck was not allowed to leave the ice.
3) The stones marking the place to score goals were placed on the ice at opposite angles to those at present.
4) There was to be no slashing.
5) There was to be no lifting the stick above the shoulder.
6) When a goal was scored, ends were changed.
7) Players had to keep ‘on side’ of his stick.
8) The forward pass was permitted.
9) Players played the entire game.
10) There was a no-replacement rule for penalized players.
11) The game had two thirty minute periods with a ten minute break.
12) The goal-keeper had to stand for the entire game.
13) Goals were decided by the goal umpires, who stood at the goalmouth and rang a handbell.
"""

let rugby = newText.replacingOccurrences(of: "puck", with: "ball").replacingOccurrences(of: "stick", with: "face").replacingOccurrences(of: "ice", with: "pitch").replacingOccurrences(of: "goals", with: "random amounts of points")

print(rugby)

// a different way to do loop with a closure

rugby.components(separatedBy: "\n").forEach({string in
    if string.starts(with: "1") || string.contains("was") {
        print(string)
    }
})

// RegEx too!

let reg = try? NSRegularExpression.init(pattern: ".*\\)", options: .caseInsensitive)

rugby.components(separatedBy: "\n").forEach({string in
    print(reg?.stringByReplacingMatches(in: string, options: [], range: NSRange(location: 0, length: string.count), withTemplate: "-"))
})

// Exercises

// 1. Create a string with a line of text, split it into component words.
// 2. Take that same string and print every third word
// 3. Find a word in the text and remove all occurances of it

